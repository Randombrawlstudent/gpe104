﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private Transform theTransform;
    private float moveHorizontal;
    private float moveVertical;


    public float speed;     // public value for a designer to edit

    // Start is called before the first frame update
    void Start()
    {
        theTransform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        moveHorizontal = Input.GetAxis("Horizontal");
        moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, moveVertical, 0);
        theTransform.position = theTransform.position + (movement * speed);     // movement formula

        if (Input.GetKeyDown(KeyCode.Space))
            transform.position = new Vector3(0, 0, 0);
    }
}
