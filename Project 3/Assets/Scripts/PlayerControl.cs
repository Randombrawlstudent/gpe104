﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{

    public float speed;
    public float rotate;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("up"))             // If statements for movement
        {
            transform.Translate(0, speed, 0);
        }
        if (Input.GetKey("down"))
        {
            transform.Translate(0, -speed, 0);
        }
        if (Input.GetKey("left"))           // if statements for rotation
        {
            transform.Rotate(0, 0, rotate);
        }
        if (Input.GetKey("right"))
        {
            transform.Rotate(0, 0, -rotate);
        }
    }
}
