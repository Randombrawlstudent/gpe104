﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour
{

    public GameObject BulletSpawner;
    public GameObject Bullet;
    public float Speed;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            GameObject TemporaryBulletSpawner;    // Spawns a temporary bullet spawner to shoot the bullet
            TemporaryBulletSpawner = Instantiate(Bullet, BulletSpawner.transform.position, BulletSpawner.transform.rotation);   // Tells unity what to spawn, where to spawn it, and with what rotation

            Rigidbody2D TemporaryRigidBody2D;
            TemporaryRigidBody2D = TemporaryBulletSpawner.GetComponent<Rigidbody2D>();

            TemporaryRigidBody2D.AddForce(transform.up * Speed);  // Applies speed to the bullet
        }
    }
}
