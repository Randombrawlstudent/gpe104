﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_scripts : MonoBehaviour
{

    public GameObject objectToDisable;
    public static bool objectDisabled = false;
    // Start is called before the first frame update
    void Start()
    {
        objectToDisable.GetComponent("Movement");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q)) {      // Function to enable and disable the status of the ship object
            if (objectDisabled == true)
            {
                objectToDisable.SetActive(true);
                objectDisabled = false;
            }
            else {
                objectToDisable.SetActive(false);
                objectDisabled = true;
            }
        
        if (Input.GetKey("escape"))       // exit function
            {
                Application.Quit();
            }
        }
    }
}
