﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform Player;    // Object or camera to follow

    public float SmoothSpeed = 0.15f;   // Speed in which the camera smooths to follow the player, so the player can see that they are moving
    public Vector3 offset;  // Camera position

    void LateUpdate()       // function for camera's position
    {
        Vector3 desiredPosition = Player.position + offset;     
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, SmoothSpeed);  
        transform.position = smoothedPosition;
    }
}
