﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // Uses scene manager to change scenes

public class Snowflake : MonoBehaviour
{

    private Scene NextScene;

    public AudioClip CollectSound;


    private void OnTriggerEnter2D(Collider2D collision)     // Loads the next scene, named NextScene, and is Scene1
    {
        AudioSource.PlayClipAtPoint(CollectSound, new Vector2(1, 1));
        SceneManager.LoadScene("CompleteScene");
    }
}
