﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public float Speed;
    private float WaitTime; // Allows designers to set how long the AI effectively 'randomly stops'
    public float StartWaitTime;
    

    public Transform Destination;
    private Transform Player;       
    public float minX;      // 4 values that allow a designer to designate where the AI wanders
    public float maxX;
    public float minY;
    public float maxY;

    private EnemyState CurrentState;

    void start()
    {
        WaitTime = StartWaitTime;
        Player = GameObject.FindGameObjectWithTag("player").GetComponent<Transform>();      // Finds the player
    }

    void Update()
    {
        switch (CurrentState)
        {
            case EnemyState.Wander:     // AI state to wander around
                {
                    transform.position = Vector2.MoveTowards(transform.position, Destination.position, Speed * Time.deltaTime); // moves the AI towards the object for its destination

                    if (Vector2.Distance(transform.position, Destination.position) < 0.2f)  // for when the enemy reaches within a cloes proximity to the object
                    {
                        if (WaitTime <= 0)  // Once the AI has waited at the position for its duration and the timer has reached 0
                        {
                            Destination.position = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY)); // Finds a new position within the bounds of a range determined by a designer
                            WaitTime = StartWaitTime;
                        }
                        else
                        {
                            WaitTime -= Time.deltaTime;
                        }
                    }

                    break;
                }
            case EnemyState.Chase:
                {
                    transform.position = Vector2.MoveTowards(transform.position, Player.position, Speed * Time.deltaTime);  // Move directly to the player

                    if(Vector2.Distance(transform.position, Player.position) > 2)       // turn this into a destroy player
                    {
                        
                    }

                    break;
                }
        }

    }
}

public enum EnemyState
{
    Wander,
    Search,
    Chase
}