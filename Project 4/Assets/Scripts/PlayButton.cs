﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // Uses scene manager to change scenes

public class PlayButton : MonoBehaviour
{
    public void LoadScene()
    {
        SceneManager.LoadScene(1);  // Loads scene #2 in the build settings (which is 1, and is the game)
    }
}