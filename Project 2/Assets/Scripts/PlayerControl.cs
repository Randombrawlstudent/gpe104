﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{

    public float speed;
    public float rotate;
    public GameObject bullet;
    public float bulletSpeed;
    public float bulletTimer = 5f;


    private Rigidbody2D m_Rigidbody2D;

    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("up"))             // If statements for movement
        {
            transform.Translate(0, speed, 0);
        }
        if (Input.GetKey("down"))
        {
            transform.Translate(0, -speed, 0);
        }
        if (Input.GetKey("left"))           // if statements for rotation
        {
            transform.Rotate(0, 0, rotate);
        }
        if (Input.GetKey("right"))
        {
            transform.Rotate(0, 0, -rotate);
        }
    }



    private void OnCollisionEnter2D(Collision2D other)  // function to delete this object when it collides with another object
    {
        if (other.gameObject.tag != "Bullet")       // Here to prevent player from being able to run into own bullets to destroy themselves
        {
            Destroy(this.gameObject);
        }
    }
    
    private void OnBecameInvisible()  // function to delete object when it leaves the screen
    {
        Destroy(this.gameObject);
    }
}