﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public Sprite Unchecked;    // Sprite for when the checkpoint has not been reached
    public Sprite Checked;      // Sprite for when the checkpoint has been reached
    public bool CheckpointReached;

    private SpriteRenderer CheckpointSpriteRenderer;

    // Start is called before the first frame update
    void Start()    // Gets the spriteRenderer Component
    {
        CheckpointSpriteRenderer = GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D other) // When the player interacts with the checkpoint
    {
        if (other.tag == "Player")
        {
            CheckpointSpriteRenderer.sprite = Checked; 
            CheckpointReached = true;
        }
    }
}
