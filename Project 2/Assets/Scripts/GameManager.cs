﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public int score = 0;
    public int lives = 3;

    public GameObject Player;
    public Transform[] spawnPoints;
    public GameObject[] enemies;
    public Transform[] PlayerSpawn;
    private int randomSpawnPoint, EnemyToSpawn;
    public int shipPercent;
    public int asteroidPercent;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        SpawnPlayer();
        InvokeRepeating("SpawnEnemy", 0f, 1f);      // Starts running the SpawnEnemy function every second
    }

    void Update()
    {
        if (GameObject.FindGameObjectWithTag("Player") == null)     // When the player object is not alive
        {
            if (lives > 0)  // Checks if lives are not less than 0
            {
                DestroyEnemies();   // Destroys all enemies function
                SpawnPlayer();      // Spawns player function
                lives = lives - 1;  // Lowers li
            }
            else   // When the player has no lives left, ends the game basically
            {

            }
        }
    }

    void SpawnEnemy()
    {
        if (GameObject.FindGameObjectsWithTag("Enemy").Length < 3)      // If there are less than 3 enemies, it spawns another
        {
            randomSpawnPoint = Random.Range(0, spawnPoints.Length);     // Finds a random spawn point from the list
            EnemyToSpawn = Random.Range(1, (asteroidPercent + 1));  // Range of numbers to pull
            if (EnemyToSpawn >= shipPercent)        // Formula for spawning asteroids or ships
            {
                Instantiate(enemies[1], spawnPoints[randomSpawnPoint].position, Quaternion.identity);   //Spawns a ship at a random position
            }
            else
            {
                Instantiate(enemies[0], spawnPoints[randomSpawnPoint].position, Quaternion.identity);   //Spawns an asteroid at a random position
            }
        }
    }

    void SpawnPlayer()  // Function to call player, which is called on start and after the player dies
    {
        Instantiate(Player, PlayerSpawn[0].position, Quaternion.identity);
    }
    void DestroyEnemies()       //Function to destroy all enemies when the player is destroyed
    {
        var enemyObjects = GameObject.FindGameObjectsWithTag("Enemy");

        for (var i = 0; i < enemyObjects.Length; i++)   // For loop to destroy all objects with the Enemy tag
        {
            Destroy(enemyObjects[i]);
        }
    }
}