﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControl : MonoBehaviour
{

    Rigidbody2D rb2d;
    GameObject Target;
    public float speed;
    Vector3 DirectionToTarget;

    private Rigidbody2D m_Rigidbody2D;      // Collision

    // Start is called before the first frame update
    void Start()
    {
        Target = GameObject.Find("Playership(Clone)");
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("Playership(Clone)") == null)      // Destroys object if player is dead
            Destroy(this.gameObject);
        Move();     // Movement
    }

    void Move()
    {
        DirectionToTarget = (Target.transform.position - transform.position).normalized;        // Finds the player's location and moves to that position
        rb2d.velocity = new Vector2(DirectionToTarget.x * speed, DirectionToTarget.y * speed);  // Finds the speed to apply to the ship
        var angle = Mathf.Atan2(DirectionToTarget.x, DirectionToTarget.y) * Mathf.Rad2Deg;      // Finds the angle the ship needs to face to reach the player
        transform.rotation = Quaternion.AngleAxis(-angle, Vector3.forward);     // Angles the ship
    }
    private void OnCollisionEnter2D(Collision2D other)  // function to delete this object when it collides with another object
    {
        if (other.gameObject.tag == "Bullet")
        Destroy(this.gameObject);
    }
    private void OnBecameInvisible()  // function to delete object when it leaves the screen
    {
        Destroy(this.gameObject);
    }
}
