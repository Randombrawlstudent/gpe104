﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{

    public bool IsGrounded;
    public BoxCollider2D bc2d;

    private Rigidbody2D rigidbody;
    private SpriteRenderer spriteRenderer;
    private Animator animator;
    private int jumps;

    public AudioClip jumpSound;
    public AudioClip LandSound;
    public float speed = 5.0f;
    public float jumpForce = 7.0f;
    public int maxJumps = 1;
    public Vector3 RespawnPoint;

    // Start is called before the first frame update
    void Start()
    {
        jumps = maxJumps;
        rigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
<<<<<<< HEAD
        RespawnPoint = transform.position;
=======
        bc2d = GetComponent<BoxCollider2D>();
>>>>>>> 2add21b4a089be69227896db1d01b745c15a4ac1
    }

    // Update is called once per frame
    void Update()
    {
        float xMovement = Input.GetAxis("Horizontal") * speed;
        rigidbody.velocity = new Vector2(xMovement, rigidbody.velocity.y);
        if (IsGrounded)
        {
            if (rigidbody.velocity.x > 0.1f)
            {
                //animator.Play("Snowman_walk");
                spriteRenderer.flipX = false;
            }
            else if (rigidbody.velocity.x < -0.1f)
            {
                //animator.Play("Snowman_Walk");
                spriteRenderer.flipX = true;
            }
        }
        if (Input.GetButtonDown("Jump") && (jumps > 0)) // If the player has jumps remaining
        {
            IsGrounded = false;
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, jumpForce);
            AudioSource.PlayClipAtPoint(jumpSound, new Vector2(1, 1));
            jumps = jumps - 1;
            animator.SetBool("Airborne", true);
        }
<<<<<<< HEAD
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, Vector2.down, 0.363f);     // draws a raycast to detect if the player is grounded
        if (hitInfo.collider != null) 
=======
        float colliderHeight = bc2d.size.y / 2f;
        Vector3 bottom = newVector(transform.position.x, transform.position.y - colliderHeight);
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, Vector2.down, 1f); // , 1f); -> colliderHeight + 0.1f);
        Vector3 debugRay = new Vector3(0, 0, colliderHeight - 0.1f,0f);)
        Debug.DrawRay(bottom, debugRay, Color.red;
        if (hitInfo.collider != null)
>>>>>>> 2add21b4a089be69227896db1d01b745c15a4ac1
        {
            if (IsGrounded == false)
            {
                AudioSource.PlayClipAtPoint(LandSound, new Vector2(1, 1));      // Plays this sound only once so the sound is not spammed while grounded
            }
            IsGrounded = true;
            animator.SetBool("Airborne", false);
            jumps = maxJumps;
        }
        animator.SetFloat("Speed", Mathf.Abs(rigidbody.velocity.x));
    }
    private void OnBecameInvisible()  // function to delete object when it leaves the screen
    {
        transform.position = RespawnPoint;
    }
    void OnTriggerEnter2D(Collider2D other)     // When the player enters the trigger that is the checkpoint
    {
        if (other.tag == "Checkpoint")
        {
            RespawnPoint = other.transform.position;
        }
    }
}