﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnBecameInvisible()  // function to delete object when it leaves the screen
    {
        Destroy(this.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision2D)  // function to delete this object when it collides with another object
    {
        if (collision2D.gameObject.tag == "Enemy")
        {
            Destroy(this.gameObject);
        }
        else
        {

        }
    }
}
