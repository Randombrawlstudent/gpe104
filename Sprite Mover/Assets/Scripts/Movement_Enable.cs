﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement_Enable : MonoBehaviour
{
    private GameObject Move;

    private Movement movement;
    // Start is called before the first frame update
    void Start()
    {
        movement = GetComponent<Movement>();        // GetComponent to get the movement component
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
           movement.enabled = !movement.enabled;        // sets the movement.enabled's status to whatever it is not
        }
    }
}