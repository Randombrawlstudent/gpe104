﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidControl : MonoBehaviour
{
    Rigidbody2D rb2d;
    GameObject Target;
    public float speed;
    Vector3 DirectionToTarget;


    // Start is called before the first frame update
    void Start()
    {
        Target = GameObject.Find("Playership(Clone)");  // Finds the player
        rb2d = GetComponent<Rigidbody2D>();
        Move();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("Playership(Clone)") == null)      // destroys object if player is dead
            Destroy(this.gameObject);
    }

    void Move()
    {
        DirectionToTarget = (Target.transform.position - transform.position).normalized;    // finds the point in which the asteroid needs to move to
        rb2d.velocity = new Vector2 (DirectionToTarget.x * speed, DirectionToTarget.y * speed); // applies speed to the point
        var angle = Mathf.Atan2(DirectionToTarget.x, DirectionToTarget.y) * Mathf.Rad2Deg;      // Finds angle in which to point the asteroid
        transform.rotation = Quaternion.AngleAxis(-angle, Vector3.forward);     // Rotates the ship based on the angle found in the previous line
    }
    private void OnCollisionEnter2D(Collision2D other)  // function to delete this object when it collides with another object
    {
        if (other.gameObject.tag == "Bullet")   // Prevents ships from being destroyed by touching each other
            Destroy(this.gameObject);
    }
    private void OnBecameInvisible()  // function to delete object when it leaves the screen
    {
        Destroy(this.gameObject);
    }
}
